# AndminLTE 3 with Ruby On Rails 6
AdminLTE 3 template for rails 6

## Documentation
| Version          | Link                                      |
| ---------------  | ----------------------------------------- |
| AdminLTE 2.4.18  | https://adminlte.io/docs/2.4/installation |
| AdminLTE 3.0.5   | https://adminlte.io/docs/3.0/             |

### Previews
| Version          | Link                                            |
| ---------------  | ----------------------------------------------- |
| AdminLTE  2.4.18 | https://adminlte.io/themes/AdminLTE/index2.html |
| AdminLTE 3.0.5   | https://adminlte.io/themes/v3/index.html        |


## Run project
```bash
# Install dependencies
bundle install

# node dependencies
yarn install

# Run rails server
rails server
```

## NOTES.md
The file `NOTES.md` contains instructions about how integrate *AdminLTE 3.0.5*
and *Rails 6*


## Requirements

- Ruby (I use ruby 2.7.1p83)
- Ruby on Rails (I use Rails 6.0.3.2)
- [NodeJS](https://nodejs.org/es/).
- [Yarn](https://yarnpkg.com/).

**Note**: I advise you to install ruby using a version manager like [rvm](https://rvm.io/rvm/install) or [rbenv](https://github.com/rbenv/rbenv). Also, I recommend the [GoRails Tutorial](https://gorails.com/setup/windows/10) that will guide you throughout the process.
